/*
* Copyright (c) 2022 Daniél Kerkmann <daniel@kerkmann.dev>
* and the contributors of the MarsValley project.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
* the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* SPDX-License-Identifier: EUPL-1.2
*/

extern crate proc_macro;

use proc_macro::TokenStream;
use proc_macro2::Span;
use quote::{quote, ToTokens};
use syn::punctuated::*;
use syn::token::*;
use syn::*;

fn filter_by_attribute_name<'a>(fields: &'a Punctuated<Field, Comma>, name: &'a str) -> &'a Field {
    fields
        .iter()
        .filter(|f| {
            f.attrs
                .iter()
                .map(|a| a.path.get_ident())
                .filter(|i| i.is_some())
                .filter(|i| i.unwrap() == name)
                .count()
                > 0
        })
        .last()
        .expect(format!("There is no field '{}'", name).as_str())
}

fn get_attribute_function_rename<'a>(field: &'a Field, attribute_name: &'a str) -> Option<String> {
    field
        .attrs
        .iter()
        .map(|f| f.parse_meta().unwrap())
        .filter(|m| {
            if let Some(ident) = m.path().get_ident() {
                ident == attribute_name
            } else {
                false
            }
        })
        .map(|m| match m {
            Meta::NameValue(name_value) => {
                if let Lit::Str(lit) = name_value.lit {
                    lit.value()
                } else {
                    panic!("{}: can only be a string!", attribute_name)
                }
            }
            _ => {
                panic!("{}: needs an argument!", attribute_name)
            }
        })
        .last()
}

fn get_attribute_function_rename_or<'a>(
    fallback: &'a str,
    field: &'a Field,
    attribute_name: &'a str,
) -> String {
    get_attribute_function_rename(field, attribute_name).unwrap_or(fallback.to_string())
}

fn get_function_name<'a>(field: &Field, name: &str) -> String {
    field
        .clone()
        .ident
        .map_or(name.to_string(), |ident| ident.to_string())
}

fn field_is_float(field: &Field) -> bool {
    let ty = field.clone().ty.to_token_stream().to_string();
    ty == "f64" || ty == "f32"
}

#[proc_macro_derive(
    MinMax,
    attributes(
        min,
        max,
        current,
        function_name,
        increase_function_name,
        increase_full_function_name,
        decrease_function_name,
        decrease_full_function_name
    )
)]
pub fn minmax_derive(input: TokenStream) -> TokenStream {
    let ast = parse_macro_input!(input as DeriveInput);
    let name = &ast.ident;
    let vis = &ast.vis;

    // builder
    let builder_name = format!("{}Builder", name);
    let builder_ident = Ident::new(&builder_name, name.span());
    // fields
    let fields = if let Data::Struct(DataStruct {
        fields: Fields::Named(FieldsNamed { ref named, .. }),
        ..
    }) = ast.data
    {
        named
    } else {
        unimplemented!();
    };

    // min
    let field_min = filter_by_attribute_name(&fields, "min");
    let field_min_ident = &field_min.ident;
    let field_min_ty = &field_min.ty;
    // max
    let field_max = filter_by_attribute_name(&fields, "max");
    let field_max_ident = &field_max.ident;
    let field_max_ty = &field_max.ty;
    // current
    let field_current = filter_by_attribute_name(&fields, "current");
    let field_current_ident = &field_current.ident;
    let field_current_ty = &field_current.ty;

    if field_min_ty != field_max_ty || field_max_ty != field_current_ty {
        panic!("'min', 'max' and 'current' must have the same type!")
    }

    // set_min
    let set_min_function_name = format!("set_{}", get_function_name(field_min, "min"));
    let set_min_function_ident = Ident::new(set_min_function_name.as_str(), Span::call_site());
    // set_max
    let set_max_function_name = format!("set_{}", get_function_name(field_max, "max"));
    let set_max_function_ident = Ident::new(set_max_function_name.as_str(), Span::call_site());
    // set_current
    let set_current_function_name = format!("set_{}", get_function_name(field_current, "current"));
    let set_current_function_ident =
        Ident::new(set_current_function_name.as_str(), Span::call_site());

    // get_min
    let get_min_function_name = format!("get_{}", get_function_name(field_min, "min"));
    let get_min_function_ident = Ident::new(get_min_function_name.as_str(), Span::call_site());
    // get_max
    let get_max_function_name = format!("get_{}", get_function_name(field_max, "max"));
    let get_max_function_ident = Ident::new(get_max_function_name.as_str(), Span::call_site());
    // get_current
    let get_current_function_name = format!("get_{}", get_function_name(field_current, "current"));
    let get_current_function_ident =
        Ident::new(get_current_function_name.as_str(), Span::call_site());

    // is_min
    let is_min_function_name =
        get_attribute_function_rename_or("is_min", &field_min, "function_name");
    let is_min_function_ident = Ident::new(is_min_function_name.as_str(), Span::call_site());
    // is_near_min
    let is_near_min_function_name =
        get_attribute_function_rename_or("is_near_min", &field_min, "function_name");
    let is_near_min_function_ident =
        Ident::new(is_near_min_function_name.as_str(), Span::call_site());
    // is_max
    let is_max_function_name =
        get_attribute_function_rename_or("is_max", &field_max, "function_name");
    let is_max_function_ident = Ident::new(is_max_function_name.as_str(), Span::call_site());
    // is_near_max
    let is_near_max_function_name =
        get_attribute_function_rename_or("is_near_max", &field_max, "function_name");
    let is_near_max_function_ident =
        Ident::new(is_near_max_function_name.as_str(), Span::call_site());
    // increase
    let increase_function_name =
        get_attribute_function_rename_or("increase", &field_current, "increase_function_name");
    let increase_function_ident = Ident::new(increase_function_name.as_str(), Span::call_site());
    // increase_full
    let increase_full_function_name = get_attribute_function_rename_or(
        "increase_full",
        &field_current,
        "increase_full_function_name",
    );
    let increase_full_function_ident =
        Ident::new(increase_full_function_name.as_str(), Span::call_site());
    // decrease
    let decrease_function_name =
        get_attribute_function_rename_or("decrease", &field_current, "decrease_function_name");
    let decrease_function_ident = Ident::new(decrease_function_name.as_str(), Span::call_site());
    // decrease_full
    let decrease_full_function_name = get_attribute_function_rename_or(
        "decrease_full",
        &field_current,
        "decrease_full_function_name",
    );
    let decrease_full_function_ident =
        Ident::new(decrease_full_function_name.as_str(), Span::call_site());

    let is_float = field_is_float(field_current);
    // output
    let zero_ident = if is_float {
        quote! {0.0}
    } else {
        quote! {0}
    };
    let getter_expanded = quote! {
            pub fn #get_min_function_ident(&self) -> #field_min_ty {
                self.#field_min_ident
            }

            pub fn #get_max_function_ident(&self) -> #field_max_ty {
                self.#field_max_ident
            }

            pub fn #get_current_function_ident(&self) -> #field_current_ty {
                self.#field_current_ident
            }
    };
    let setter_expanded = quote! {
            pub fn #set_min_function_ident(&mut self, #field_min_ident: #field_min_ty) -> Result<&mut Self, MinMaxError> {
                if #field_min_ident > self.#field_max_ident {
                    Err(MinMaxError::MinIsHigherThanMax)
                } else {
                    self.#field_min_ident = #field_min_ident;
                    Ok(self)
                }
            }

            pub fn #set_max_function_ident(&mut self, #field_max_ident: #field_max_ty) -> Result<&mut Self, MinMaxError> {
                if self.#field_min_ident > #field_max_ident {
                    Err(MinMaxError::MaxIsLowerThanMin)
                } else {
                    self.#field_max_ident = #field_max_ident;
                    Ok(self)
                }
            }

            pub fn #set_current_function_ident(&mut self, #field_current_ident: #field_current_ty) -> Result<&mut Self, MinMaxError> {
                if #field_current_ident < self.#field_min_ident {
                    Err(MinMaxError::CurrentIsLowerThanMin)
                } else if #field_current_ident > self.#field_max_ident {
                    Err(MinMaxError::CurrentIsHigherThanMax)
                } else {
                    self.#field_current_ident = #field_current_ident;
                    Ok(self)
                }
            }
    };
    let special_getter_expanded = if is_float {
        quote! {
            pub fn #is_near_min_function_ident(&self) -> bool {
                (self.#field_current_ident - self.#field_min_ident).abs() <= #field_current_ty::EPSILON
            }

            pub fn #is_near_max_function_ident(&self) -> bool {
                (self.#field_max_ident - self.#field_current_ident).abs() <= #field_current_ty::EPSILON
            }
        }
    } else {
        quote! {
            pub fn #is_min_function_ident(&self) -> bool {
                self.#field_current_ident <= self.#field_min_ident
            }

            pub fn #is_max_function_ident(&self) -> bool {
                self.#field_current_ident >= self.#field_max_ident
            }
        }
    };
    let expanded = quote! {
        use minmax::MinMaxError;

        #[derive(Debug)]
        #vis struct #builder_ident {
            #field_min_ident: #field_min_ty,
            #field_max_ident: #field_max_ty,
            #field_current_ident: #field_current_ty,
        }

        impl #builder_ident {
            pub fn new(#field_min_ident: #field_min_ty, #field_max_ident: #field_max_ty) ->  Result<Self, MinMaxError> {
                if #field_min_ident > #field_max_ident {
                    Err(MinMaxError::MinIsHigherThanMax)
                } else {
                    Ok(Self {
                        #field_min_ident,
                        #field_max_ident,
                        #field_current_ident: #field_min_ident,
                    })
                }
            }

            #getter_expanded
            #setter_expanded

            pub fn build(&self) -> #name {
                #name {
                    #field_min_ident: self.#field_min_ident,
                    #field_max_ident: self.#field_max_ident,
                    #field_current_ident: self.#field_current_ident,
                }
            }
        }

        impl #name {
            #getter_expanded
            #setter_expanded
            #special_getter_expanded

            pub fn #increase_function_ident(&mut self, amount: #field_current_ty) -> &mut Self {
                let amount = if amount < #zero_ident {
                    #zero_ident
                } else {
                    amount
                };
                self.#field_current_ident += if self.#field_max_ident - self.#field_current_ident < amount {
                    self.#field_max_ident - self.#field_current_ident
                } else {
                    amount
                };
                self
            }

            pub fn #increase_full_function_ident(&mut self) -> &mut Self {
                self.#field_current_ident = self.#field_max_ident;
                self
            }

            pub fn #decrease_function_ident(&mut self, amount: #field_current_ty) -> &mut Self {
                let amount = if amount < #zero_ident {
                    #zero_ident
                } else {
                    amount
                };
                self.#field_current_ident -= if self.#field_current_ident - self.#field_min_ident < amount {
                    self.#field_current_ident - self.#field_min_ident
                } else {
                    amount
                };
                self
            }

            pub fn #decrease_full_function_ident(&mut self) -> &mut Self {
                self.#field_current_ident = self.#field_max_ident;
                self
            }
        }
    };
    expanded.into()
}

//#[cfg(test)]
//mod tests {
//    use approx::assert_relative_eq;
//
//    use super::*;
//
//    const STEP: f32 = 25.0;
//    const CURRENT: f32 = 50.0;
//    const MIN: f32 = 0.0;
//    const MAX: f32 = 100.0;
//
//    #[test]
//    fn damage_normal() {
//        let mut health = Health {
//            current: CURRENT,
//            min: MIN,
//            max: MAX,
//        };
//        health.damage(STEP);
//        assert_relative_eq!(health.current, CURRENT - STEP);
//    }
//
//    #[test]
//    fn damage_current_health() {
//        let mut health = Health {
//            current: CURRENT,
//            min: MIN,
//            max: MAX,
//        };
//        health.damage(CURRENT);
//        assert_relative_eq!(health.current, health.min);
//    }
//
//    #[test]
//    fn damage_max_health() {
//        let mut health = Health {
//            current: CURRENT,
//            min: MIN,
//            max: MAX,
//        };
//        health.damage(MAX);
//        assert_relative_eq!(health.current, health.min);
//    }
//
//    #[test]
//    fn damage_min_limit() {
//        let mut health = Health {
//            current: CURRENT,
//            min: MIN,
//            max: MAX,
//        };
//        health.damage(std::f32::MIN);
//        assert_relative_eq!(health.current, health.current);
//    }
//
//    #[test]
//    fn damage_max_limit() {
//        let mut health = Health {
//            current: CURRENT,
//            min: MIN,
//            max: MAX,
//        };
//        health.damage(std::f32::MAX);
//        assert_relative_eq!(health.current, health.min);
//    }
//
//    #[test]
//    fn heal_normal() {
//        let mut health = Health {
//            current: CURRENT,
//            min: MIN,
//            max: MAX,
//        };
//        health.heal(STEP);
//        assert_relative_eq!(health.current, CURRENT + STEP);
//    }
//
//    #[test]
//    fn heal_current_health() {
//        let mut health = Health {
//            current: CURRENT,
//            min: MIN,
//            max: MAX,
//        };
//        health.heal(CURRENT);
//        assert_relative_eq!(health.current, CURRENT + CURRENT);
//    }
//
//    #[test]
//    fn heal_max_health() {
//        let mut health = Health {
//            current: CURRENT,
//            min: MIN,
//            max: MAX,
//        };
//        health.heal(MAX);
//        assert_relative_eq!(health.current, health.max);
//    }
//
//    #[test]
//    fn heal_min_limit() {
//        let mut health = Health {
//            current: CURRENT,
//            min: MIN,
//            max: MAX,
//        };
//        health.heal(std::f32::MIN);
//        assert_relative_eq!(health.current, health.current);
//    }
//
//    #[test]
//    fn heal_max_limit() {
//        let mut health = Health {
//            current: CURRENT,
//            min: MIN,
//            max: MAX,
//        };
//        health.heal(std::f32::MAX);
//        assert_relative_eq!(health.current, health.max);
//    }
//
//    #[test]
//    fn heal_full() {
//        let mut health = Health {
//            current: CURRENT,
//            min: MIN,
//            max: MAX,
//        };
//        health.heal_full();
//        assert_relative_eq!(health.current, health.max);
//    }
//
//    #[test]
//    fn die() {
//        let mut health = Health {
//            current: CURRENT,
//            min: MIN,
//            max: MAX,
//        };
//        health.die();
//        assert_relative_eq!(health.current, health.min);
//    }
//
//    #[test]
//    fn is_dead() {
//        let mut health = Health {
//            current: CURRENT,
//            min: MIN,
//            max: MAX,
//        };
//        assert!(!health.is_dead());
//        health.die();
//        assert!(health.is_dead());
//        health.heal_full();
//        assert!(!health.is_dead());
//    }
//
//    #[test]
//    fn is_full() {
//        let mut health = Health {
//            current: CURRENT,
//            min: MIN,
//            max: MAX,
//        };
//        assert!(!health.is_full());
//        health.heal_full();
//        assert!(health.is_full());
//        health.die();
//        assert!(!health.is_full());
//    }
//}
