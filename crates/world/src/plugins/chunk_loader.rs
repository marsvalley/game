/*
* Copyright (c) 2022 Daniél Kerkmann <daniel@kerkmann.dev>
* and the contributors of the MarsValley project.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
* the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* SPDX-License-Identifier: EUPL-1.2
*/

use bevy::prelude::*;
use bevy::tasks::{AsyncComputeTaskPool, Task};
use bevy::utils::HashMap;
use bevy_rapier3d::prelude::*;
use futures_lite::future;

use crate::components::Ground;
use crate::config::{CHUNK_LOAD_RADIUS, CHUNK_SIZE};

pub struct ChunkLoaderPlugin;

#[derive(Default)]
pub struct ChunkLoader {
    pub chunks_loaded: HashMap<(i64, i64), Entity>,
}

pub struct ChunkLoadEvent {
    pub offset_x: i64,
    pub offset_z: i64,
}

pub struct ChunkUnloadEvent {
    pub offset_x: i64,
    pub offset_z: i64,
}

#[derive(Component)]
struct ComputeGround(Task<Ground>);

#[derive(Component, Default)]
struct Chunk;

impl Plugin for ChunkLoaderPlugin {
    fn build(&self, app: &mut App) {
        app.add_event::<ChunkLoadEvent>()
            .add_event::<ChunkUnloadEvent>()
            .insert_resource(ChunkLoader::default())
            .add_startup_system(init_chunks)
            .add_system(load_chunk_system)
            .add_system(unload_chunk_system)
            .add_system(spawn_chunks);
    }
}

fn init_chunks(mut events: EventWriter<ChunkLoadEvent>) {
    for x in -CHUNK_LOAD_RADIUS..=CHUNK_LOAD_RADIUS {
        for z in -CHUNK_LOAD_RADIUS..=CHUNK_LOAD_RADIUS {
            events.send(ChunkLoadEvent {
                offset_x: x,
                offset_z: z,
            })
        }
    }
}

fn load_chunk_system(
    mut commands: Commands,
    mut events: EventReader<ChunkLoadEvent>,
    mut chunk_loader: ResMut<ChunkLoader>,
) {
    let thread_pool = AsyncComputeTaskPool::get();
    for event in events.iter() {
        let position_x = event.offset_x;
        let position_z = event.offset_z;
        let task = thread_pool.spawn(Ground::new(position_x, position_z));
        let entity = commands.spawn().insert(ComputeGround(task)).id();
        chunk_loader
            .chunks_loaded
            .insert((position_x, position_z), entity);
    }
}

fn unload_chunk_system(
    mut commands: Commands,
    mut events: EventReader<ChunkUnloadEvent>,
    mut chunk_loader: ResMut<ChunkLoader>,
) {
    for event in events.iter() {
        let position = &(event.offset_x, event.offset_z);
        if let Some(entity) = chunk_loader.chunks_loaded.get(position) {
            commands.entity(entity.to_owned()).despawn();
            chunk_loader.chunks_loaded.remove(position);
        }
    }
}

fn spawn_chunks(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
    mut transform_tasks: Query<(Entity, &mut ComputeGround)>,
) {
    for (entity, mut task) in &mut transform_tasks {
        if let Some(ground) = future::block_on(future::poll_once(&mut task.0)) {
            commands
                .spawn_bundle(PbrBundle {
                    mesh: meshes.add(ground.mesh),
                    material: materials.add(StandardMaterial {
                        base_color: Color::SALMON,
                        perceptual_roughness: 1.0,
                        ..Default::default()
                    }),
                    transform: Transform::from_xyz(
                        (ground.offset_x * CHUNK_SIZE) as f32,
                        0.0,
                        (ground.offset_z * CHUNK_SIZE) as f32,
                    ),
                    ..Default::default()
                })
                .insert(RigidBody::Fixed)
                .insert(ground.collider)
                .insert(Chunk::default());

            commands.entity(entity).remove::<ComputeGround>();
        }
    }
}
