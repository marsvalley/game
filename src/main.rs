/*
* Copyright (c) 2022 Daniél Kerkmann <daniel@kerkmann.dev>
* and the contributors of the MarsValley project.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
* the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* SPDX-License-Identifier: EUPL-1.2
*/

#![allow(clippy::needless_update)]

use autodefault::autodefault;
use bevy::diagnostic::{FrameTimeDiagnosticsPlugin, LogDiagnosticsPlugin};
use bevy::prelude::*;
use bevy::window::PresentMode;
use bevy_rapier3d::prelude::*;

use client::plugins::client::ClientPlugin;
use network::network::NetworkMode;
use server::plugins::server::ServerPlugin;
use world::chunk_loader::ChunkLoaderPlugin;

pub mod components;
pub mod plugins;
pub mod states;

use components::*;
use plugins::*;
use states::*;

const TITLE: &str = "MarsValley Game";

#[autodefault]
#[cfg(not(tarpaulin_include))]
fn main() {
    // for now we just render an empty window, we will add plugins, systems, startup systems,
    // entities and components to the app
    App::new()
        // samples for antialiasing (4 is max)
        .insert_resource(Msaa { samples: 4 })
        .insert_resource(WindowDescriptor {
            title: TITLE.to_string(),
            present_mode: PresentMode::Mailbox,
        })
        .add_plugins(DefaultPlugins)
        .add_plugin(LogDiagnosticsPlugin::default())
        // TODO: Deactivated for anti spaming
        .add_plugin(FrameTimeDiagnosticsPlugin::default())
        .add_plugin(RapierPhysicsPlugin::<NoUserData>::default())
        // Multiplayer
        .add_state(NetworkMode::None)
        .add_plugin(ServerPlugin)
        .add_plugin(ClientPlugin)
        .add_startup_system(init)
        // now we can exit the game with esc
        .add_system(bevy::window::close_on_esc)
        .add_state(MouseState::Uncatched)
        .add_plugin(CameraPlugin)
        .add_plugin(PlayerPlugin)
        .add_plugin(ChunkLoaderPlugin)
        //.add_plugin(WorldPlugin)
        .run();
}

#[autodefault]
#[cfg(not(tarpaulin_include))]
fn init(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
) {
    // player,
    commands
        .spawn_bundle(PbrBundle {
            mesh: meshes.add(Mesh::from(shape::Capsule {
                radius: 0.5,
                depth: 1.0,
            })),
            material: materials.add(Color::rgb(0.3, 0.5, 0.2).into()),
            transform: Transform::from_xyz(50.0, 10.0, 50.0),
        })
        .insert(RigidBody::Dynamic)
        .insert(LockedAxes::ROTATION_LOCKED_X | LockedAxes::ROTATION_LOCKED)
        .insert(Collider::capsule(
            Vect::new(0.0, -0.5, 0.0),
            Vect::new(0.0, 0.5, 0.0),
            0.5,
        ))
        .insert(ExternalForce::default())
        .insert(ExternalImpulse::default())
        .insert(PlayerMovement::default())
        .insert(GlobalTransform::default())
        .insert(Name::new("Player"));

    // DirectionalLight
    const SIZE: f32 = 1000.0;
    commands
        .spawn_bundle(DirectionalLightBundle {
            directional_light: DirectionalLight {
                shadow_projection: OrthographicProjection {
                    left: -SIZE,
                    right: SIZE,
                    bottom: -SIZE,
                    top: SIZE,
                    near: -SIZE,
                    far: SIZE,
                },
                shadows_enabled: true,
                illuminance: 10000.0,
            },
            transform: Transform::from_xyz(500.0, 200.0, 500.0)
                .looking_at(Vec3::new(0.0, 0.0, 0.0), Vec3::Y),
        })
        .insert(Name::new("Directional Light"));

    // camera
    commands
        .spawn_bundle(Camera3dBundle {
            transform: Transform::from_xyz(300.0, 200.0, 300.0)
                .looking_at(Vec3::new(50.0, 5.0, 50.0), Vec3::Y),
        })
        .insert(CameraMotion::default())
        .insert(CameraMovement::default())
        .insert(Name::new("Camera"));
}
