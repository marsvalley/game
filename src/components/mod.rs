/*
* Copyright (c) 2022 Daniél Kerkmann <daniel@kerkmann.dev>
* and the contributors of the MarsValley project.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
* the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* SPDX-License-Identifier: EUPL-1.2
*/
pub mod camera;
pub mod debug;
pub mod energy;
pub mod health;
pub mod hunger;
//pub mod network;
pub mod oxygen;
pub mod player;
pub mod stamina;
pub mod thirst;

pub use camera::*;
pub use debug::*;
pub use energy::*;
pub use health::*;
pub use hunger::*;
//pub use network::*;
pub use oxygen::*;
pub use player::*;
pub use stamina::*;
pub use thirst::*;
