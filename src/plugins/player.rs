/*
* Copyright (c) 2022 Daniél Kerkmann <daniel@kerkmann.dev>
* and the contributors of the MarsValley project.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
* the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* SPDX-License-Identifier: EUPL-1.2
*/

use bevy::prelude::*;
use bevy_rapier3d::prelude::{ExternalForce, ExternalImpulse};

use world::chunk_loader::{ChunkLoadEvent, ChunkLoader};
use world::config::{CHUNK_LOAD_RADIUS, CHUNK_SIZE};

use crate::components::PlayerMovement;
use crate::states::MouseState;

pub struct PlayerPlugin;

#[derive(Default, PartialEq, Eq)]
struct ChunkPosition {
    pub position_x: i64,
    pub position_z: i64,
}

impl From<&Transform> for ChunkPosition {
    fn from(transform: &Transform) -> Self {
        Self {
            position_x: transform.translation.x as i64 / CHUNK_SIZE,
            position_z: transform.translation.z as i64 / CHUNK_SIZE,
        }
    }
}

#[derive(Default)]
struct LatestPlayerChunkPosition(ChunkPosition);

impl Plugin for PlayerPlugin {
    fn build(&self, app: &mut App) {
        app.add_system_set(
            SystemSet::on_update(MouseState::PlayerCamera).with_system(player_movement_system),
        )
        .insert_resource(LatestPlayerChunkPosition::default())
        .add_system(add_chunks_system);
    }
}

fn add_chunks_system(
    player_query: Query<&Transform, With<PlayerMovement>>,
    mut latest_player_chunk_position: ResMut<LatestPlayerChunkPosition>,
    chunk_loader: Res<ChunkLoader>,
    mut chunk_load_events: EventWriter<ChunkLoadEvent>,
) {
    let player_chunk_position: ChunkPosition = player_query.single().into();
    if latest_player_chunk_position.0 != player_chunk_position {
        for x in -CHUNK_LOAD_RADIUS..=CHUNK_LOAD_RADIUS {
            for z in -CHUNK_LOAD_RADIUS..=CHUNK_LOAD_RADIUS {
                let position_x = player_chunk_position.position_x + x;
                let position_z = player_chunk_position.position_z + z;

                if !chunk_loader
                    .chunks_loaded
                    .contains_key(&(position_x, position_z))
                {
                    chunk_load_events.send(ChunkLoadEvent {
                        offset_x: position_x,
                        offset_z: position_z,
                    })
                }
            }
        }
        latest_player_chunk_position.0 = player_chunk_position;
    }
}

fn player_movement_system(
    input: Res<Input<KeyCode>>,
    mut query: Query<(
        &mut ExternalForce,
        &mut ExternalImpulse,
        &mut PlayerMovement,
        &Transform,
    )>,
) {
    if input.is_changed() {
        let (mut forces, mut impulses, mut player_movement, transform) = query.single_mut();

        if input.pressed(KeyCode::LControl) {
            player_movement.speed = 100.0;
        } else {
            player_movement.speed = 10.0;
        }

        // Forward: KeyCode::W
        // Backward: KeyCode::S
        if input.pressed(KeyCode::W) {
            forces.force += transform.rotation * Vec3::X;
        } else if input.pressed(KeyCode::S) {
            forces.force -= transform.rotation * Vec3::X;
        }

        // Left: KeyCode::A
        // Right: KeyCode::D
        if input.pressed(KeyCode::A) {
            forces.force -= transform.rotation * Vec3::Z;
        } else if input.pressed(KeyCode::D) {
            forces.force += transform.rotation * Vec3::Z;
        }

        if !input.pressed(KeyCode::W)
            && !input.pressed(KeyCode::A)
            && !input.pressed(KeyCode::S)
            && !input.pressed(KeyCode::D)
        {
            forces.force = Vec3::ZERO;
        }

        // TODO: check if the player is on the ground
        if input.just_pressed(KeyCode::Space) {
            impulses.impulse.y = player_movement.speed;
        }

        forces.force = forces
            .force
            .clamp(Vec3::new(-10.0, -10.0, -10.0), Vec3::new(10.0, 10.0, 10.0));
    }
}
